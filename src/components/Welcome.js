import React from 'react';

function Welcome(props) {
    return (
        <div>
            <h1>Home Page</h1>
            <h2>{props.appName}</h2>
        </div>
    )
}

export default Welcome;