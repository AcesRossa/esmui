import React, {Component} from 'react';
import EmployeeRestService from "../services/EmployeeRestService";
import {Redirect} from "react-router-dom";

export default class AddEmployee extends Component {
    constructor(props) {
        super(props);

        this.onChangeFname = this.onChangeFname.bind(this);
        this.onChangeLname = this.onChangeLname.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeSalary = this.onChangeSalary.bind(this);
        this.onChangeId = this.onChangeId.bind(this);
        this.saveEmployee = this.saveEmployee.bind(this);

        this.state = {
            id: 0,
            fname: "",
            lname: "",
            email: "",
            salary: 0,
        }
    }

    onChangeFname(e) {
        this.setState({
            fname: e.target.value
        });
    }

    onChangeLname(e) {
        this.setState({
            lname: e.target.value
        });
    }

    onChangeId(e) {
        this.setState({
            id: e.target.value
        });
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        });
    }

    onChangeSalary(e) {
        this.setState({
            salary: e.target.value
        });
    }

    saveEmployee() {
        var data = {
            id: this.state.id,
            fname: this.state.fname,
            lname: this.state.lname,
            email: this.state.email,
            salary: this.state.salary        }

        // {"id":1,"fname":"deirdre","lname":"geary","salary":0,"email":"d@i.ie"}
        EmployeeRestService.create(data)
            .then(response => {
                this.setState({
                    // id: response.data.id,
                    // fname: response.data.fname,
                    // lname: response.data.lname,
                    // email: response.data.email,
                    // salary: response.data.salary,
                    submitted: true
                });
            })
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    render() {
        if(this.state.submitted) {
            return <Redirect to="home" />
        }
        return (
            <form>
                <h1>Add Employees</h1>
                <div className="form-group">
                    <label htmlFor="exampleFirstname">First Name</label>
                    <input type="text" className="form-control" id="exampleFirstname" aria-describedby="fnameHelp"
                           placeholder="Enter name" value={this.state.fname} onChange={this.onChangeFname}/>
                    <small id="fnameHelp" className="form-text text-muted">We'll never share your details with anyo
                        else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleLastname">Last Name</label>
                    <input type="text" className="form-control" id="exampleLastname" aria-describedby="lnameHelp"
                           placeholder="Enter name" value={this.state.lname} onChange={this.onChangeLname}/>
                    <small id="lnameHelp" className="form-text text-muted">We'll never share your details with anyone
                        else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Email address</label>
                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                           placeholder="Enter email" value={this.state.email} onChange={this.onChangeEmail}/>
                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone
                        else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleSalary">Salary</label>
                    <input type="number" className="form-control" id="exampleSalary" aria-describedby="emailHelp"
                           placeholder="Enter salary" value={this.state.salary} onChange={this.onChangeSalary}/>
                    <small id="emailSalary" className="form-text text-muted">We'll never share your salary with anyone
                        else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleid">Id</label>
                    <input type="number" className="form-control" id="exampleid" aria-describedby="emailid"
                           placeholder="Enter id" value={this.state.id} onChange={this.onChangeId}/>
                </div>
                <div className="form-group">

                    <input type="button" className="form-control" value="Save" onClick={this.saveEmployee}/>
                </div>
            </form>)
    }
}