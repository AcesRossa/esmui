import React, {Component} from 'react';

class About extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name : "Allstate",
            year : "2020",
            apiStatus : "Unknown"
        }
    }

    async componentDidMount() {
        let url = "http://localhost:8080/api/status"
        let textData;
        try {
            let response = await fetch(url);
            textData = await response.text();
            console.log('The data is: ' + textData)
        } catch (e) {
            console.log("The Error is: ", e.toString())
        }

        this.setState({apiStatus : textData});
    }

    render() {
        return (
            <>
                <h1>Name is {this.state.name}</h1>
                <h2>Year is {this.state.year}</h2>
                <h3>Status is {this.state.apiStatus}</h3>
            </>
        )
    }
}

export default About;