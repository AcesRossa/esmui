import React, {Component} from 'react';
import EmployeeRestService from "../services/EmployeeRestService";

export default class ListEmployees extends Component {
    constructor(props) {
        super(props);

        this.state = {
            employees: [],
            currentIndex: -1
        }
    }

    retrieveEmployees() {
        EmployeeRestService.getAll()
            .then(response => {
                this.setState({employees: response.data})
                console.log(response.data)
            })
            .catch(e => {
                console.log(e)
            })
    }

    componentDidMount() {
        this.retrieveEmployees()
    }

    render() {
        const {employees, currentIndex} = this.state
        return (
            <div className="col-md-6">
                <h4>Employees List</h4>

                <ul className="list-group">
                    {employees &&
                    employees.map((employee, index) => (
                        <li
                            className={"list-group-item " + (index === currentIndex ? "active" : "")}
                            // onClick={() => this.setActiveTutorial(tutorial, index)}
                            key={index}
                        >
                            {employee.fname} {employee.lname}
                        </li>
                    ))}
                </ul>
            </div>
        )
    }

}