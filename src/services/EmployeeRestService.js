import http from '../http-common';

export class EmployeeRestService {
    getAll() {
        return http.get("/all");
    }

    get(id) {
        return http.get(`/find/${id}`)
    }

    create(data) {
        return http.post("/save", data)
    }
}

export default new EmployeeRestService();