import React from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import { Switch, Route, Link } from "react-router-dom";
import AddEmployee from "./components/AddEmployee"
import ViewEmployees from "./components/ViewEmployees"
import ListEmployees from "./components/ListEmployees"
import Welcome from "./components/Welcome"
import About from "./components/About";

function App() {
  return (
    <div>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="/home" className="navbar-brand">
          Home
        </a>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={"/employees"} className="nav-link">
              Employees
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/add"} className="nav-link">
              Add Employee
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/about"} className="nav-link">
              About
            </Link>
          </li>
        </div>
      </nav>

      <div className="container mt-3">
        <Switch>
          <Route exact path={["/", "/home"]} component={Welcome} />
          <Route exact path={["/", "/employees"]} component={ListEmployees} />
          <Route exact path="/add" component={AddEmployee} />
          <Route exact path="/about" component={About} />
          <Route path="/employees/:id" component={ViewEmployees} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
